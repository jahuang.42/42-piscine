/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_solver.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/25 17:25:11 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/25 19:33:55 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_checker(char **table, int x, int y, int value);

char	**ft_solver(char **table, int x, int y, int value)
{
	int size;

	printf("%d%d\n", x , y);
	printf("%s%d\n", "is ok", ft_checker(table, x, y, value));

	size = 52;
	if (ft_checker(table, x, y, value) == 1)
	{
		table[x][y] = value;
		if (x == 4 && y == 4)
			return (table);
		if (y == 4)
		{
			ft_solver(table, (x + 1), 1, 49);
		}
		printf("%s%d\n", "OK number", value);
		ft_solver(table, x, (y + 1), 49);
	}
	if (ft_checker(table, x, y, value) == 0)
	{
		printf("%s%d\n", "NG number", value);

		if (value < size)
			ft_solver(table, x, y, (value + 1));
		if (value == size)
		{
			if (y == 1 && x == 1)
			{
				return (table);
			}
			if (y == 1)
			{
				x -= 1;
				y = 4;
				ft_solver(table, x, y, 49);
			}
		}
		if (x == 1 && y == 1 && value == 52)
			return (table);
	}
	return (table);
}
