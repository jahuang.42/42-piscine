/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_checker.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/25 17:29:31 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/25 17:46:22 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_check_row(char **table, int x, int y, int value);
int	ft_check_col(char **table, int x, int y, int value);
int	ft_check_dup(char **table, int x, int y, int value);

int	ft_checker(char **table, int x, int y, int value)
{
	if (ft_check_dup(table, x, y, value) == 1)
		return (1);
	return (0);
}

int	ft_check_row(char **table, int x, int y, int value)
{
}

int	ft_check_col(char **table, int x, int y, int value)
{
}

int	ft_check_dup(char **table, int x, int y, int value)
{
	int i;
	int j;

	i = 1;
	while (i < y)
	{
		if (table[x][i] == table[x][y])
			return (0);
		i++;
	}
	j = 1;
	while (j < x)
	{
		if (table[j][y] == table[x][y])
			return (0);
		j++;
	}
	return (1);
}
