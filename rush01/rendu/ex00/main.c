/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/25 17:20:03 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/25 19:50:33 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

char	**ft_init_table(int size, char *str);
char	**ft_solver(char **table, int x, int y, int value);
int		ft_strlen(char *str);
int		ft_error(char *str);

int		ft_input_checker(char *str)
{
	return (1);
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_print_table(char **table)
{
	int i;
	int j;

	i = 0;
	while (i < 6)
	{
		j = 0;
		while (j < 6)
		{
			ft_putchar(table[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

int		main(int argc, char **argv)
{
	char	**table;
	int		k;
	int		l;

	if (argc != 2 || ft_strlen(argv[1]) == 0 || ft_error(argv[1]) == 0)
	{
		write(1, "Error\n", 6);
		return (1);
	}
	table = NULL;
	table = ft_init_table(4, argv[1]);
	ft_solver(table, 1, 1, '1');
	ft_print_table(table);
	return (0);
}
