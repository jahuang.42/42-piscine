/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_solver.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/25 17:25:11 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/25 17:45:53 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_checker(char **table, int x, int y, int value);

char	**ft_solver(char **table, int x, int y, int value)
{
	int size;

	size = '4';
	if (ft_checker(table, x, y, value) == 1)
	{
		table[x][y] = value;
		if (x == 4 && y == 4)
			return (table);
		if (y == 4)
		{
			ft_solver(table, (x + 1), 1, 49);
		}
		ft_solver(table, x, (y + 1), 49);
	}
	if (ft_checker(table, x, y, value) == 0)
	{
		if (value < size)
		{
			value += 1;
			ft_solver(table, x, y, value);
		}
		if (value == size)
		{
			if (y == 1 && x == 1)
			{
				return (table);
			}
			if (y == 1)
			{
				x -= 1;
				y = 4;
				ft_solver(table, x, y, 49);
			}
		}
		if (x == 1 && y == 1 && value == 52)
			return (table);
	}
	return (table);
}
