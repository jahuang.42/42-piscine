/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/27 16:38:14 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/27 16:38:18 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int	*tab;
	int range;
	int i;

	range = max - min;
	i = 0;
	if (range <= 0)
	{
		tab = 0;
		return (tab);
	}
	if (!(tab = malloc(range * sizeof(*tab))))
		return (0);
	while (i < range)
	{
		tab[i] = min + i;
		i++;
	}
	return (tab);
}
