#include <stdio.h>
#include <stdlib.h>

int ft_nbrlen(int nbr, int baselen)
{
	int count;
	unsigned int unbr;

	if (nbr < 0)
		unbr = -nbr;
	else
		unbr = nbr;
	count = 0;
	while (unbr / baselen != 0)
	{
		count++;
		unbr = unbr / baselen;
	}
	return (count + 1);
}

int		ft_baselen(char *base)
{
	int	i;
	int	j;

	i = 0;
	while (base[i])
	{
		if (base[i] == '+' || base[i] == '-' ||
				(base[i] >= 9 && base[i] <= 13))
			return (0);
		j = i + 1;
		while (base[j] != '\0')
		{
			if (base[j] == base[i] && base[j] != '\0')
				return (0);
			j++;
		}
		i++;
	}
	return (i);
}

int		find_index_inbase(char c, char *base)
{
	int	i;

	i = 0;
	while (base[i])
	{
		if (base[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

char	*ft_itoa_writer(unsigned int unbr, int nbrlen, int baselen, char *base_to)
{
		char *result;

		result = 0;
		if (!(result = (char*)malloc((nbrlen + 1) * sizeof(char))))
			return (0);
		result[nbrlen] = '\0';
		while (nbrlen > 0)
		{
			result[nbrlen - 1] = base_to[unbr % baselen];
			unbr = unbr / baselen;
			nbrlen--;
		}
		return (result);
}
