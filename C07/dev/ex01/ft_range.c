#include <stdio.h>
#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int	*tab;
	int range;
	int i;

	range = max - min;
	i = 0;
	if (range <= 0)
	{
		tab = 0;
		return (tab);
	}
	tab = malloc(range * sizeof(*tab));
	while (i < range)
	{
		tab[i] = min + i;
		i++;
	}
	return (tab);
}

int		main(int argc, char **argv)
{
	int *a; 
	a = ft_range(atoi(argv[1]), atoi(argv[2]));
	int i = 0;

	while (i < atoi(argv[2]) - atoi(argv[1]))
	{
		printf("%d\n", a[i]);
		i++;
	}
}

