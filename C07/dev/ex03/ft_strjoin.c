#include <stdio.h>
#include <stdlib.h>

int		ft_strlen(char *str)
{
	int counter;

	counter = 0;
	while (str[counter])
	{
		counter++;
	}
	return (counter);
}

int		ft_count_totallen(int size, char **strs, char *sep)
{
	int i;
	int totallen;
	int seplen;

	i = 0;
	totallen = 0;
	seplen = ft_strlen(sep);
	while (i < size)
	{
		totallen += ft_strlen(strs[i]);
		if (i != size - 1)
			totallen += seplen;
		i++;
	}
	return (totallen);
}

int    ft_strcat(char *str1, char *str2)
{
	int i;
	i = 0;
	while (*str2)
	{
		str1[i] = *str2;
		i++;
		str2++;
	}
	return (i);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	int i;
	int j;
	int totallen;
	char *result;

	i = 0;
	j = 0;
	totallen = ft_count_totallen(size, strs, sep);
	result = (char *)malloc((totallen + 1) * sizeof(char));
	while (i < size)
	{
		j += ft_strcat(&result[j], strs[i]);
		if (i != size - 1)
			j += ft_strcat(&result[j], sep);
		i++;
	}
	result[j] = '\0';
	return (result);
}

int		main(int argc, char **argv)
{
	char *hello;
	printf("%d\n", argc);

	hello = ft_strjoin(argc, argv, ", ");
	printf("%s", hello);
	free(hello);
}
