#include <stdlib.h>

char *ft_strdup(char *src)
{
	int srclen;
	int i;
	char *dest;

	srclen = 0;
	i = 0;
	while (src[srclen] != '\0')
		srclen++;
	dest = malloc(srclen * sizeof(*dest));
	while (src[i] != '\0')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}
