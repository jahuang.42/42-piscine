#include <stdio.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*ft_strstr(char *str, char *charset)
{
	int		i;

	i = 0;
	while (str[i] && charset[i])
	{
		if (str[i] != charset[i])
			return ();
		i++;
	}
	return (str);
}

int		ft_word_count(char *str, char *charset)
{
	int		diff;
	int		count;
	int		i;

	i = 0;
	count = 0;
	while (str[i])
	{
		diff = (ft_strstr_after(&str[i], charset) - &str[i]);
		printf("%s%d\n", "diff" , diff);
		printf("%s%d\n", "strlen" , ft_strlen(charset));
		if (diff != 0 && diff == ft_strlen(charset))
		{
			count++;
			i += diff;
		}
		else
			i++;
	}
	return (count);
}



int		main(int argc, char **argv)
{
	
	printf("%d\n", ft_word_count(argv[1], argv[2]));
}
