/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/23 12:18:20 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/27 16:58:32 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	int i;
	int *holder;
	int size;

	i = 0;
	size = max - min;
	if (max - min <= 0)
	{
		range = 0;
		return (0);
	}
	if (!(holder = (int *)malloc(sizeof(int) * (max - min))))
		return (-1);
	while (i < size)
	{
		holder[i] = i + min;
		i++;
	}
	*range = holder;
	return (size);
}
