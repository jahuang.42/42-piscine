/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/27 16:49:22 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/27 16:54:29 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_nbrlen(int nbr, int baselen);
int		ft_baselen(char *base);
int		find_index_inbase(char c, char *base);
char	*ft_itoa_writer(unsigned int unbr, int nbrlen, int baselen, char *base);

int		ft_atoi_base(char *str, char *base)
{
	int				i;
	int				negatif;
	unsigned int	result;

	i = 0;
	negatif = 1;
	result = 0;
	if (ft_baselen(base) <= 1)
		return (0);
	while ((str[i] >= 9 && str[i] <= 13) || str[i] == ' ')
		i++;
	while (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			negatif = -negatif;
		i++;
	}
	while (find_index_inbase(str[i], base) != -1)
	{
		result = result * ft_baselen(base) + find_index_inbase(str[i], base);
		i++;
	}
	return (result * negatif);
}

char	*ft_itoa_base(int nbr, char *base_to)
{
	int				nbrlen;
	int				baselen;
	unsigned int	unbr;
	char			*result;

	baselen = ft_baselen(base_to);
	nbrlen = ft_nbrlen(nbr, baselen);
	if (nbr < 0)
	{
		unbr = -nbr;
		nbrlen++;
		result = ft_itoa_writer(unbr, nbrlen, baselen, base_to);
		result[0] = '-';
	}
	else
		result = ft_itoa_writer(nbr, nbrlen, baselen, base_to);
	return (result);
}

char	*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	int		atoi_nbr;
	char	*result;

	if (ft_baselen(base_from) <= 1 || ft_baselen(base_to) <= 1)
		return (0);
	atoi_nbr = ft_atoi_base(nbr, base_from);
	result = ft_itoa_base(atoi_nbr, base_to);
	return (result);
}
