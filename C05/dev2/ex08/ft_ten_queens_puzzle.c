/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ten_queens_puzzle.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/25 15:43:07 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/25 15:46:31 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putstr(char *str)
{
	while (*str)
	{
		write(1, str, 1);
		str++;
	}
}

int		ft_solver(char *board, char col, char row)
{
	if (col == '0')
		return ;
	if (row == '9')
		return (board);
	if (col < '9')
		board[col] = row;
}

int		is_available(int *board, int y)
{
	return (0);
}

int		ft_ten_queens_puzzle(void)
{
	char board[10];
	char row;
	char col;

	row = '0';
	col = 0;
	board[0] = row;
	ft_solver(board, col + 1, row);
}
