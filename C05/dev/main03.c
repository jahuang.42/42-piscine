#include <unistd.h>

int ft_recursive_power(int nb, int power);
void ft_putnbr(int nbr)
{
	int print;

	if (nbr / 10 != 0)
		ft_putnbr(nbr / 10);
	print = nbr % 10 + 48;
	write(1, &print, 1);
}

int main(void)
{
	int i = 0;

	while (i < 6)
	{
		ft_putnbr(i);
		ft_putnbr(ft_recursive_power(3, i));
		write(1, "\n", 1);
		i++;
	}
}
