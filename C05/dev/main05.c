#include <unistd.h>
#include <stdlib.h>

int ft_sqrt(int nb);

void ft_putnbr(int nbr)
{
	int print;

	if (nbr / 10 != 0)
		ft_putnbr(nbr / 10);
	print = nbr % 10 + 48;
	write(1, &print, 1);
}

int main(int argc, char **argv)
{
	(void) argc;
	int i = 0;

	ft_putnbr(atoi(argv[1]));
	ft_putnbr(ft_sqrt(atoi(argv[1])));
	write(1, "\n", 1);
	i++;
}
