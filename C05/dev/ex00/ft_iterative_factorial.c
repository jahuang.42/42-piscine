int ft_iterative_factorial(int nb)
{
	if (nb < 0)
		return (0);
	if (nb == 1 || nb == 0)
		return (1);
	while (nb != 1)
	{
		nb = nb * (nb - 1);
		nb--;
	}
	return (nb);
}
