int ft_is_prime(int nb)
{
	int i;
	int max;

	i = 2;
	max = 46341;
	if (nb == 0 || nb == 1)
		return (0);
	while (i < nb / 2 && i < max)
	{
		if (nb % i == 0)
			return (0);
		i++;
	}
	return (1);
}

int     ft_find_next_prime(int nb)
{
    while (ft_is_prime(nb) == 0)
        nb++;
	return (nb);
}
