#include <stdio.h>

int ft_fibonacci(int nb);

int main()
{
	int i;

	i = 0;
	while (i < 10)
	{
		printf("%d\n", ft_fibonacci(i));
		i++;
	}
};
