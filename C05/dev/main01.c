#include <unistd.h>

int ft_recursive_factorial(int nb);
void ft_putnbr(int nbr)
{
	int print;

	if (nbr / 10 != 0)
		ft_putnbr(nbr / 10);
	print = nbr % 10 + 48;
	write(1, &print, 1);
}

int main(void)
{
	ft_putnbr(ft_recursive_factorial(0));
	write(1, "\n", 1);
	ft_putnbr(ft_recursive_factorial(1));
	write(1, "\n", 1);
	ft_putnbr(ft_recursive_factorial(2));
	write(1, "\n", 1);
	ft_putnbr(ft_recursive_factorial(3));
	write(1, "\n", 1);
	ft_putnbr(ft_recursive_factorial(4));
	write(1, "\n", 1);
	ft_putnbr(ft_recursive_factorial(5));
	write(1, "\n", 1);
}


