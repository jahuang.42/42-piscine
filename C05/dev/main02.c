#include <unistd.h>
#include <stdlib.h>

int ft_iterative_power(int nb, int power);

void ft_putnbr(int nbr)
{
	int print;

	if (nbr / 10 != 0)
		ft_putnbr(nbr / 10);
	print = nbr % 10 + 48;
	write(1, &print, 1);
}

int main(int argc, char **argv)
{
	(void) argc;
		ft_putnbr(ft_iterative_power(atoi(argv[1]), atoi(argv[2])));
		write(1, "\n", 1);
}
