/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/21 10:31:51 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/21 10:36:09 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcat(char *dest, char *src)
{
	int				i;
	int				j;
	unsigned char	tmp;

	i = 0;
	j = 0;
	while (dest[i] != '\0')
		i++;
	while (src[j] != '\0')
	{
		tmp = src[j];
		dest[i + j] = tmp;
		j++;
	}
	dest[i + j] = '\0';
	return (dest);
}
