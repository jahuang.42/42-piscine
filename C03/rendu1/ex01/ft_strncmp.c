/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/21 10:31:42 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/21 10:35:03 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned char	tmp1;
	unsigned char	tmp2;
	unsigned int	i;

	i = 0;
	while ((s1[i] != '\0') && (s1[i] == s2[i]) && i < n)
		i++;
	if (i == n)
		return (0);
	tmp1 = s1[i];
	tmp2 = s2[i];
	return (tmp1 - tmp2);
}
