#include <unistd.h>
#include <string.h>

int ft_strlcat(char *dest, char *src, int size);

void putnbr(int n)
{
	if (n < 0)
	{
		n = -n;
		write(1, "-", 1);
	}
	int i;
	i = 0;
	if (n / 10 != 0)
		putnbr(n/10);
	i = 48 + (n % 10);
	write(1, &i, 1);
}

void putstr(char *str)
{
	int i = 0;
	while (str[i] != '\0')
	{
		write(1, &str[i], 1);
		i++;
	}
}

int	main(void)
{
	char de[20] = "abcde";
	char *hay = "1234567";
	int i;


	putstr(de);
	write(1, "\n", 1);
	i = ft_strlcat(de, hay, 3);
	putnbr(i);
	write(1, "\n", 1);
	putstr(de);
	write(1, "\n", 1);
}

