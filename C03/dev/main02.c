#include <unistd.h>
#include <string.h>

char *ft_strcat(char *dest, char *src);

void putnbr(int n)
{
	if (n < 0)
	{
		n = -n;
		write(1, "-", 1);
	}
	int i;
	i = 0;
	if (n / 10 != 0)
		putnbr(n/10);
	i = 48 + (n % 10);
	write(1, &i, 1);
}

void putstr(char *str)
{
	int i = 0;
	while (str[i] != '\0')
	{
		write(1, &str[i], 1);
		i++;
	}
}

int	main(void)
{
	char de[10] = "def";
	char *sr = "abc";
	char dex[10] = "def";
	char *srx = "abc";

	putstr(de);
	write(1, "\n", 1);
	strcat(de, sr);
	putstr(de);
	write(1, "\n", 1);
	putstr(dex);
	write(1, "\n", 1);
	ft_strcat(dex, srx);
	putstr(dex);
}
