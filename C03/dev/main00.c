#include <unistd.h>
#include <string.h>

int ft_strncmp(char *s1, char *s2, unsigned int n);

void putnbr(int n)
{
	if (n < 0)
	{
		n = -n;
		write(1, "-", 1);
	}
	int i;
	i = 0;
	if (n / 10 != 0)
		putnbr(n/10);
	i = 48 + (n % 10);
	write(1, &i, 1);
}

int	main(void)
{
	int i;
	i = ft_strncmp("adkdkd", "ad", 3);
	putnbr(i);
	write(1, "\n", 1);
	i = strncmp("adkdkd", "ad", 3);
	putnbr(i);
	write(1, "\n", 1);
	i = ft_strncmp("adkdkd", "ad", 2);
	putnbr(i);
	write(1, "\n", 1);
	i = strncmp("adkdkd", "ad", 2);
	putnbr(i);
	write(1, "\n", 1);
}

