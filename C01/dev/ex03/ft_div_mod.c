#include <unistd.h>

void ft_div_mod(int a, int b, int *div, int *mod)
{
	int d;
	int m;

	d = a / b;
	m = a % b;
	*div = d;
	*mod = m;
}

int main(void)
{
	int i;
	int devi;
	int modu;

	ft_div_mod( 3, 3, &devi, &modu);

	i = devi + 48;
	write(1, &i, 1);
	}
