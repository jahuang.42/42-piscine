#include <unistd.h>

void ft_sort_int_tab(int *tab, int size)
{
	int *cur_min;
	int tmp;
	int i;
	int j;

	i = 0;
	while (i < size)
	{
		cur_min = &tab[i];
		j = i + 1;
		while (j < size)
		{
			if (*cur_min > tab[j])
			{
				cur_min = &tab[j];
			}
			j++;
		}
		tmp = *cur_min; 
		*cur_min = tab[i];
		tab[i] = tmp;
		i++;
	}
}

int main()
{
	int array[5];
	array[0] = 3;
	array[1] = 4;
	array[2] = 2;
	array[3] = 0;
	array[4] = 5;

	int h;
	h = array[1]+48;
	write(1,&h, 1);
	write(1, "\n",1);
	ft_sort_int_tab(array, 5);
	int g;
	g = 0;
	while (g < 5)
	{
		h = array[g]+48;
		write(1,&h, 1);
		g++;
	}

}





