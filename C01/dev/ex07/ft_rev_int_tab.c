#include <unistd.h>
void ft_rev_int_tab(int *tab, int size)
{
	int i;

	i = 0;
	while (i < size - 1)
	{
		int tmp;

		tmp = tab[i];
		tab[i] = tab[size-1];
		tab[size-1] = tmp;
		i++;
		size--;
	}
}

int main(void)
{
	int tab[5];
	int i;
	i = 0;
	while(i < 5)
	{
		tab[i] = i;
		i++;
	}
	int h;
	h = tab[0] + 48;
	write(1, &h, 1);
	h = tab[1] + 48;
	write(1, &h, 1);
	h = tab[2] + 48;
	write(1, &h, 1);

	ft_rev_int_tab(tab, 5);
	h = tab[0] + 48;
	write(1, &h, 1);
	h = tab[1] + 48;
	write(1, &h, 1);
	h = tab[2] + 48;
	write(1, &h, 1);
}
