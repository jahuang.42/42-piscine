#include <unistd.h>

void ft_ft(int *nbr)
{
	*nbr = 42;
}

int main(void)
{
	int a;
	ft_ft(&a);

	int toprint = a + 6;
	write(1, &toprint, 1);
}
