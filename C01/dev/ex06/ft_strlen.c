#include <unistd.h>
int		ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

int main(void)
{
	int i;

	i = ft_strlen("1234567890123456789012345678901234567890123456789");
	write(1, &i, 1);
}
