#include <unistd.h>
void	ft_ultimate_div_mod(int *a, int *b)
{
	int div;
	int mod;

	div = *a / *b;
	mod = *a % *b;
	*a = div;
	*b = mod;
}

int		main(void)
{
	int i = 8;
	int j = 5;

	int k = i + 48;
	int l = j + 48;
	write(1, &k, 1);
	write(1, &l, 1);
	ft_ultimate_div_mod(&i, &j);

	k = i + 48;
	l = j + 48;
	write(1, &k, 1);
	write(1, &l, 1);
}
	
