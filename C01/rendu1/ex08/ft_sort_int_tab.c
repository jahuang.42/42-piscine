/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/15 18:31:05 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/15 18:34:23 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_sort_int_tab(int *tab, int size)
{
	int *cur_min;
	int tmp;
	int i;
	int j;

	i = 0;
	while (i < size)
	{
		cur_min = &tab[i];
		j = i + 1;
		while (j < size)
		{
			if (*cur_min > tab[j])
			{
				cur_min = &tab[j];
			}
			j++;
		}
		tmp = *cur_min;
		*cur_min = tab[i];
		tab[i] = tmp;
		i++;
	}
}
