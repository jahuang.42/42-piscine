/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlowcase.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/18 13:14:38 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/18 13:19:53 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_lower(char c)
{
	if (c >= 97 && c <= 122)
		return (1);
	return (0);
}

int		is_upper(char c)
{
	if (c >= 65 && c <= 90)
		return (1);
	return (0);
}

char	*ft_strlowcase(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (is_upper(str[i]) == 1 && is_lower(str[i]) == 0)
			str[i] = str[i] + 32;
		i++;
	}
	return (str);
}
