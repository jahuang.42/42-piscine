/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/18 13:17:00 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/25 12:01:51 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>
int		is_num(char c)
{
	if (c >= 48 && c <= 57)
		return (1);
	return (0);
}

int		is_up(char c)
{
	if (c >= 65 && c <= 90)
		return (1);
	return (0);
}

int		is_low(char c)
{
	if (c >= 97 && c <= 122)
		return (1);
	return (0);
}

int		is_num_alph(char c)
{
	if (is_num(c) == 1 || is_up(c) == 1 || is_low(c) == 1)
		return (1);
	return (0);
}

char	*ft_strcapitalize(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		if (is_up(str[i]) == 1)
			str[i] += 32;
		if (i == 0 || is_num_alph(str[i - 1]) == 0)
		{
			if (is_low(str[i]) == 1)
				str[i] = str[i] - 32;
		}
		i++;
	}
	return (str);
}

int main()
{
	char *str = ft_strcapitalize("abc");
	printf("%s", str);
}

