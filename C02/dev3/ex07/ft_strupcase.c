/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strupcase.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/18 13:12:50 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/18 13:31:00 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_lower(char c)
{
	if (c >= 97 && c <= 122)
		return (1);
	return (0);
}

int		is_upper(char c)
{
	if (c >= 65 && c <= 90)
		return (1);
	return (0);
}

char	*ft_strupcase(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (is_lower(str[i]) == 1 && is_upper(str[i]) == 0)
			str[i] = str[i] - 32;
		i++;
	}
	return (str);
}
