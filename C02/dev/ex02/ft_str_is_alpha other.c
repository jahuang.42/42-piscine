#include <unistd.h>

int		ft_str_is_alpha(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (!((str[i] >= 65 && str[i] <= 91) || (str[i] >= 97 && str[i] <= 122)))
			return (0);
		i++;
	}
	return(1);
}

int		main(void)
{
	int hello;

	hello = ft_str_is_alpha("abckdladsfklasdflkw") + 48;
	write(1, &hello, 1);
	write(1, "\n", 1);
	hello = ft_str_is_alpha("bdd833sfklasdflkw") + 48;
	write(1, &hello, 1);
	write(1, "\n", 1);
}
