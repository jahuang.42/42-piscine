#include <unistd.h>
unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size)
{
	int i;
	int	srclen;

	i = 0;
	srclen = 0;
	while (src[srclen] != '\0')
		srclen++;
	while (i < size - 1)
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (srclen);
}
