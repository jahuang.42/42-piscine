#include <unistd.h>

int	is_num(char c)
{
	if (c >= 48 && c <= 57)
		return (1);
	return (0);
}

int	is_up(char c)
{
	if (c >= 65 && c <= 90)
		return (1);
	return (0);
}

int	is_low(char c)
{
	if (c >= 97 && c <= 122)
		return (1);
	return (0);
}

int	is_num_alph(char c)
{
	if (is_num(c) == 1 || is_up(c) == 1 || is_low(c))
		return (1);
	return (0);
}

char	ft_char_up(char c)
{	
	c = c - 32;
	return (c);
}

char	*ft_strcapitalize(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		if (is_up(str[i]) == 1)
		   	str[i] += 32;
		if (i == 0 || is_num_alph(str[i - 1]) == 0)
		{
			if (is_low(str[i]) == 1)
				str[i] = ft_char_up(str[i]);
		}
		i++;
	}
	return (str);
}

int main()
{
    char *str;
    str = ft_strcapitalize("abcde");
    
}
