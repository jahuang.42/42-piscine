/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/18 13:18:03 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/18 13:30:53 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putstr_non_printable(char *str)
{
	int		i;
	char	*hexadec;

	i = 0;
	hexadec = "0123456789abcdef";
	while (str[i] != '\0')
	{
		if (str[i] < 32 || str[i] >= 127)
		{
			write(1, "\\", 1);
			write(1, &hexadec[str[i] / 16], 1);
			write(1, &hexadec[str[i] % 16], 1);
		}
		else
			write(1, &str[i], 1);
		i++;
	}
}
