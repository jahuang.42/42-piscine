/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_alpha.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/18 12:36:33 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/18 12:42:51 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_lower(char c)
{
	if (c >= 97 && c <= 122)
		return (1);
	return (0);
}

int		is_upper(char c)
{
	if (c >= 65 && c <= 90)
		return (1);
	return (0);
}

int		ft_str_is_alpha(char *str)
{
	int i;

	i = 0;
	if (str[0] == '\0')
		return (1);
	while (str[i] != '\0')
	{
		if (is_upper(str[i]) == 0 && is_lower(str[i]) == 0)
			return (0);
		i++;
	}
	return (1);
}
