/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/22 16:55:25 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/22 19:50:00 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putstr(char *str)
{
	while (*str)
	{
		write(1, str, 1);
		str++;
	}
}

int		ft_strcmp(char *s1, char *s2)
{
	int	i;

	i = 0;
	while ((s1[i] != '\0') && (s1[i] == s2[i]))
		i++;
	return (s1[i] - s2[i]);
}

void	ft_sort(int count, char **str)
{
	int		i;
	int		j;
	char	**curr_min;
	char	*tmp;

	i = 1;
	while (i < count)
	{
		curr_min = &str[i];
		j = i + 1;
		while (j < count)
		{
			if (ft_strcmp(*curr_min, str[j]) > 0)
				curr_min = &str[j];
			j++;
		}
		tmp = *curr_min;
		*curr_min = str[i];
		str[i] = tmp;
		i++;
	}
	return ;
}

int		main(int argc, char **argv)
{
	int	i;

	i = 1;
	ft_sort(argc, argv);
	while (i < argc)
	{
		ft_putstr(argv[i]);
		write(1, "\n", 1);
		i++;
	}
	return (0);
}
