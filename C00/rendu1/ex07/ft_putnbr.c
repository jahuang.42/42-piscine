/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/14 18:30:25 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/14 18:30:39 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	int printnb;

	if (nb == -2147483648)
	{
		ft_putchar('-');
		nb = -(nb / 10);
		ft_putnbr(nb);
		ft_putnbr(8);
		return ;
	}
	if ((nb / 10) != 0)
	{
		ft_putnbr(nb / 10);
	}
	printnb = nb % 10 + 48;
	ft_putchar(printnb);
}
