#include <unistd.h>

void	ft_print_alphabet(void)
{
	int i;

	i = 26;
	while (i > 0)
	{
		int alpha;
		alpha = i + 96;
		write(1, &alpha, 1);
		i--;
	}
}

int main(void)
{
	ft_print_alphabet();
	return (0);
}
