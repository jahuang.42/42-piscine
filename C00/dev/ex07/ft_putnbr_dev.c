#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	int printnb;

	if (nb == -2147483648)
	{
		ft_putchar('-');
		nb = -(nb / 10);
		ft_putnbr(nb);
		ft_putnbr(8);
		return ;
	}
	if ((nb / 10) != 0)
	{
		ft_putnbr(nb / 10);
	}
	printnb = nb % 10 + 48;
	ft_putchar(printnb);
}

int		main(void)
{
	ft_putnbr(8);
	ft_putchar('\n');
	ft_putnbr(38);
	ft_putchar('\n');
	ft_putnbr(-2147483648);
	ft_putchar('\n');
	ft_putnbr(2147483647);
	ft_putchar('\n');
}


