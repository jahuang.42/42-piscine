#include <unistd.h>

void	ft_print_alphabet(void)
{
	int i;

	i = 0;
	while (i < 26)
	{
		int alpha;
		alpha = i + 97;
		write(1, &alpha, 1);
		i++;
	}
}

int main(void)
{
	ft_print_alphabet();
	return (0);
}
