#include <unistd.h>

void	ft_print_tab(int *tab, int n)
{
	int i;

	i = 0;
	while (i < n)
	{
		write(1, &tab[i], 1);
		i++;
	}
}


void ft_increment(int *tab, int n, int index)
{
	while (index < n)
	{
		while (tab[index] != 10 - n + index + 48)
		{
			ft_print_tab(tab, n);
			write(1, "\n", 1);
			tab[index] = tab[index] + 1;
		}
		index++;
	}
}	



void	print_combn(int n)
{
	int	i;
	int	tab[n];
	int	index;
	
	i = 0;
	while (i < n)
	{
		tab[i] = i + 48;
		i++;
	}
	while (tab[0] <= (10 - n + 48))
	{
		index = 1;
		ft_increment(tab, n, index);
		tab[0] = tab[0] + 1;
		write(1, "\n", 1);
	}
}

int	main()
{
	print_combn(3);
	write(1, "\n", 1);
	print_combn(6);
}
