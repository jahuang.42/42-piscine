/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/15 09:49:50 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/15 11:01:07 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print_tab(int *tab, int n)
{
	int i;

	i = 0;
	while (i < n)
	{
		write(1, &tab[i], 1);
		i++;
	}
}

void	ft_increment(int *tab, int index, int n)
{
	while (tab[index] < (10 - n - index))
	{
		tab[index] = tab[index] + 1;
		write(1, &tab[index], 1);
		ft_print_tab(tab, n);
	}
	index--;
	if (index < 0)
		return ;
	ft_increment(tab, index - 1, n);
}

void	ft_print_combn(int n)
{
	int tab[n];
	int i;
	int index;

	i = 0;
	index = n - 1;
	while (i < n)
	{
		tab[i] = i + 48;
		i++;
	}
	ft_print_tab(tab, n);
	while (tab[0] != 48 + 10 - n)
	{
		ft_increment(tab, index, n);
	}
}

int		main(void)
{
	ft_print_combn(4);
}
