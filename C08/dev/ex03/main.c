#include "ft_point.h"
#include <stdio.h>

void set_point(t_point *point)
{
	point->x = 42;
	point->y = 21;
}
int main(void)
{
	t_point point;
	t_point PP;
	set_point(&point);
	set_point(&PP);

	printf("%d\n", point.x);
	printf("%d\n", point.y);
	printf("%d\n", PP.x);
	printf("%d\n", PP.y);

	return (0);
}
