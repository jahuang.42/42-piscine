int	ft_baselen(char *base)
{
	int	i;
	int	j;

	while (base[i] != '\0')
	{
		if (base[i] == '+' || base[i] == '-')
			return (0);
		j = i + 1;
		while (base[j] != '\0')
		{
			if (base[j] == base[i] && base[j] != '\0')
				return (0);
			j++;
		}
		i++;
	}
	return (i);
}

int find_index_inbase(char c, char *base)
{
	int	i;
	
	i = 0;
	while (base[i] != '\0')
	{
		if (base[i] == c)
			return (i);
		i++;
	}
	return (-1);
}


int ft_atoi_base(char *str, char *base)
{
	int	i;
	int	negatif;
	int baselen;
	unsigned int result;

	i = 0;
	negatif = 1;
	baselen = ft_baselen(base);
	result = 0;
	while ((str[i] >= 9 && str[i] <= 13) || str[i] == ' ')
	   i++;
	while (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			negatif = -negatif;
		i++;
	}
	while (find_index_inbase(str[i], base) != -1)
	{
		result = result * baselen;
		result = result + find_index_inbase(str[i], base);
		i++;
	}
	return (result * negatif);
}
