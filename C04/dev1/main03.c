#include <unistd.h>
#include <stdlib.h>

void	ft_putnbr(int nb);

int		ft_atoi(char *str);

int		main(void)
{
	ft_putnbr(atoi("-2147483648"));
	write(1, "\n", 1);
	ft_putnbr(ft_atoi("-2147483648"));
	write(1, "\n", 1);
	
	ft_putnbr(atoi("   ----+21478"));
	write(1, "\n", 1);
	ft_putnbr(ft_atoi("   ----+21478"));
	write(1, "\n", 1);
	ft_putnbr(atoi(" ---  +--+1234ab567"));
	write(1, "\n", 1);
	ft_putnbr(ft_atoi(" ---+--+1234ab567"));
	write(1, "\n", 1);
}
