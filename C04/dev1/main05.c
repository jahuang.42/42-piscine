#include <unistd.h>
#include <stdlib.h>

void	ft_putnbr(int nb);

int		ft_atoi_base(char *str, char *base);

int		main(void)
{
	ft_putnbr(ft_atoi_base("1101", "01"));
	write(1, "\n", 1);
	ft_putnbr(ft_atoi_base("100101011101", "01"));
	write(1, "\n", 1);
	ft_putnbr(ft_atoi_base("-10000000000000000000000000000000", "01"));
	write(1, "\n", 1);
}
