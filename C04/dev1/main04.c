#include <unistd.h>

void	ft_putnbr_base(int nbr, char *base);

int		main(void)
{
	ft_putnbr_base(234, "0123456789");
	write(1, "\n", 1);
	ft_putnbr_base(23, "01");
	write(1, "\n", 1);
	ft_putnbr_base(-2147483648, "0123456789");
	write(1, "\n", 1);
	ft_putnbr_base(-2147483648, "poneyvif");
	write(1, "\n", 1);
}
