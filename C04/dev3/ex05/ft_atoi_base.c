/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 15:54:38 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/27 10:01:51 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_baselen(char *base)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	while (base[i] != '\0')
	{
		if (base[i] == '+' || base[i] == '-' || 
				(base[i] >= 9 && base[i] <= 13))
			return (0);
		j = i + 1;
		while (base[j] != '\0')
		{
			if (base[j] == base[i] && base[j] != '\0')
				return (0);
			j++;
		}
		i++;
	}
	return (i);
}

int		find_index_inbase(char c, char *base)
{
	int	i;

	i = 0;
	while (base[i])
	{
		if (base[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

int		ft_atoi_base(char *str, char *base)
{
	int				i;
	int				negatif;
	unsigned int	result;

	i = 0;
    negatif = 1;
	result = 0;
	if (ft_baselen(base) <= 1)
		return (0);
	while ((str[i] >= 9 && str[i] <= 13) || str[i] == ' ')
		i++;
	while (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			negatif = -negatif;
		i++;
	}
	while (find_index_inbase(str[i], base) != -1)
	{
		result = result * ft_baselen(base) + find_index_inbase(str[i], base);
		i++;
	}
    return (result * negatif);
}
