/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 15:50:23 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/20 16:03:44 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_nbr_printer(unsigned int unbr, char *base, int baselen)
{
	if (unbr / baselen != 0)
		ft_nbr_printer(unbr / baselen, base, baselen);
	write(1, &base[unbr % baselen], 1);
}

int		ft_baselen(char *base)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (base[i] != '\0')
	{
		if (base[i] == '+' || base[i] == '-')
			return (0);
		j = i + 1;
		while (base[j] != '\0')
		{
			if (base[j] == base[i] && base[j] != '\0')
				return (0);
			j++;
		}
		i++;
	}
	return (i);
}

void	ft_putnbr_base(int nbr, char *base)
{
	int				baselen;
	int				i;
	unsigned int	unbr;

	baselen = ft_baselen(base);
	i = 0;
	if (baselen <= 1)
	{
		return ;
	}
	if (nbr < 0)
	{
		write(1, "-", 1);
		unbr = -nbr;
		ft_nbr_printer(unbr, base, baselen);
		return ;
	}
	unbr = nbr;
	ft_nbr_printer(unbr, base, baselen);
}
