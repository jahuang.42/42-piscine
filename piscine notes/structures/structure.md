```
struct s_point
{
	int x;
	int y;
};

struct s_point	a;
struct s_point b;

a.x = 123;
a.y = 42;

b = a;
```

```
typedef struct s_point
{
	int x;
	int y;
}		t_point;
```

ptr = &a;
`ptf->x = 10;` == `(*ptr).x = 10`
