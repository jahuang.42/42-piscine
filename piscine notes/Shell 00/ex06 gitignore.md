git
https://git-scm.com/docs/git-status

-s short
--ignored

grep

sed or cut

others solution
```
git status -s --ignored | grep '!!' | sed 's/!! //'
```

```
git status -s --ignored | grep '!!' | cut -d ' ' -f 2
```

git ls-file -io --ex