```
all:
	cc -o hello main.c fct.c

coucou:
	echo XX
	echo JJ
```

`make coucou`

variables:

```
SRCS = main.c fct.c

all:
	cc -o hello ${SRCS}

```


1. compile each file
	-c
2. linking
	-o

how do we do in Makefile?

```
SRC = main.c fct.c

OBJS = ${SRCS:.c=.o}

all:	${OBJS}	// les besoins
	cc -o hello ${OBJS}
```

only when it's necessary to link?
```
SRC = main.c fct.c

OBJS = ${SRCS:.c=.o}

hello:	${OBJS}	// les besoins
	cc -o hello ${OBJS}
```