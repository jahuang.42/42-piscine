```
SRC = main.c fct.c

OBJS = ${SRCS:.c=.o}

NAME = hello

CC = cc

RM = rm -f

CFLAGS = -Wall -g

.c.o:
	${CC} ${CFLAGS} -c $< -o ${<:.c=.o}

${NAME}:	${OBJS}	// les besoins
	${CC} -o ${NAME} ${OBJS}

all:	${NAME}

clean:
	${RM} ${OBJS}
	
fclean:	clean
	${RM} ${NAME}

re:	fclean all

.PHONY:	all clean fclean re // tell makfile that we don't want to interprete
```
