
`typedef int XYZ`

It's add during the compiling phase, but not the preprocessing phase. So there is also SCOPE limitations.

```
typedef int*	int_p;
#define intp	int*;

int	*a, b, c; === intp a, b, c;
int_p a, b, c;
```