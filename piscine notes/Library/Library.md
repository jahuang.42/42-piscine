- Create C Objects
`gcc -c YOURFILE.c` 
to create `.o` files (a C object that we can use to combine into exe file.)

- Create Library
`ar` create and maintain library archives
`ar -rc` 
-r      Replace or add the specified files to the archive.  If the archive does not exist a new archive file is created.  Files that replace existing files do not change the order of the files within the archive.  New files are appended to the archive unless one of the options -a, -b or -i is specil fied.
-c      Whenever an archive is created, an informational message to that effect is written to standard error.  If the -c option is specified, ar creates the archive silently.
file name must be : `libXXX.a` lib for library, XXX for your library name, .a for static library (vs dynamic).

- Use it and compile it with main
`gcc main.c -L. -lXXX`

- Indexing
`ranlib libXXX.a` 
add or update the table of contents of archive libraries