What does the @ symbol mean in a file's permission settings?


@ signifies that the file has extended attributes. Those attributes are usually used to signify that the file came from a package, was downloaded from the internet, etc.

ls -al@ imap.a
will show you the extended attributes that are saved for that file.