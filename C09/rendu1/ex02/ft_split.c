/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/28 15:17:15 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/28 17:32:13 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_is_char_in_set(char c, char *charset)
{
	while (*charset)
	{
		if (*charset == c)
			return (1);
		charset++;
	}
	return (0);
}

int		ft_word_count(char *str, char *charset)
{
	int		i;
	int		word_count;
	char	*prev_sep;
	char	*next_sep;

	prev_sep = str - 1;
	next_sep = str - 1;
	i = 0;
	word_count = 0;
	while (str[i])
	{
		if (ft_is_char_in_set(str[i], charset) == 1)
			next_sep = &str[i];
		if (next_sep - prev_sep > 1)
			word_count++;
		prev_sep = next_sep;
		i++;
	}
	if (&str[i] - prev_sep > 1)
		word_count++;
	return (word_count);
}

char	*ft_strdup(char *str, int strlen)
{
	int		i;
	char	*temp;

	if (!(temp = (char *)malloc((strlen + 1) * sizeof(char))))
		return (0);
	i = 0;
	while (i < strlen)
	{
		temp[i] = str[i];
		i++;
	}
	temp[i] = '\0';
	return (temp);
}

int		ft_strlen(char *str, char *charset)
{
	int	counter;

	counter = 0;
	while (*str && ft_is_char_in_set(*str, charset) == 0)
	{
		counter++;
		str++;
	}
	return (counter);
}

char	**ft_split(char *str, char *charset)
{
	char	**array;
	int		word_count;
	int		i;
	int		strlen;

	word_count = ft_word_count(str, charset);
	if (!(array = (char **)malloc((word_count + 1) * sizeof(char *))))
		return (0);
	i = 0;
	while (i < word_count)
	{
		while (ft_is_char_in_set(*str, charset) == 1)
			str++;
		strlen = ft_strlen(str, charset);
		array[i] = ft_strdup(str, strlen);
		i++;
		str = str + strlen + 1;
	}
	array[i] = 0;
	return (array);
}
