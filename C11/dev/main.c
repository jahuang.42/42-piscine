#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>



void	ft_foreach(int *tab, int length, void(*f)(int));

void	ft_putnbr(int nbr)
{
	int print;
	if (nbr / 10 != 0)
		ft_putnbr(nbr / 10);
	print = nbr + '0';
	write(1, &print, 1);
}

int		main(int argc, char **argv)
{
	int 	hello[] = { 1, 2, 3}; 

	ft_foreach(hello, 3, &ft_putnbr);
}
