/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/29 07:35:19 by jahuang           #+#    #+#             */
/*   Updated: 2020/10/29 07:36:10 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		*ft_map(int *tab, int length, int(*f)(int))
{
	int i;
	int *holder;

	i = 0;
	if (!(holder = malloc(length * sizeof(int))))
		return (0);
	while (i < length)
	{
			holder[i] = (*f)(tab[i]);
			i++;
	}
	return (holder);
}
